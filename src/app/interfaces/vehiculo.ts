export interface Vehiculo {
    placa: Number;
    marca: string;
    modelo: string;
    cedula: string;
    nombre: string;
    soat: Date;
    polizaresp: Date;
    tarjetaop: Date;
    todo: Date;
    tecno: Date;
    preventiva: Date;
}
