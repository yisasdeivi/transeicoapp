import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VehiculoService } from 'src/app/servicios/vehiculo.service';
import {Vehiculo} from '../../interfaces/vehiculo';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-documento',
  templateUrl: './documento.page.html',
  styleUrls: ['./documento.page.scss'],
})
export class DocumentoPage implements OnInit {
    placa: string;
    vehiculo : Vehiculo;
    vehiculoVen : Vehiculo;

  constructor(public loadingController: LoadingController,private vehiculoservice :VehiculoService,private activatedroute:ActivatedRoute) {

   }

  async ngOnInit() {
    const loading = await this.loadingController.create({
      message: 'Cargando',
    });
    await loading.present();
    this.placa= this.activatedroute.snapshot.paramMap.get('placa');
    this.getVehiculoporplaca();
    this.getVehiculoporplacaMes();
    await loading.dismiss();
  
  }
   
  getVehiculoporplaca(){
    this.vehiculoservice.getplacaVehiculo(this.placa).subscribe((Vehiculo) =>{
      this.vehiculo=Vehiculo[0];       
       console.log(this.vehiculo.soat);
    });
  }

  getVehiculoporplacaMes(){
    this.vehiculoservice.getPlacaVehiculoxmes(this.placa).subscribe((Vehiculo) =>{
      this.vehiculoVen=Vehiculo[0];       

    });
  }
  

}
