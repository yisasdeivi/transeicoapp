import { NgModule,LOCALE_ID} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { registerLocaleData } from '@angular/common';

    // importar locales
    import localeEsCo from '@angular/common/locales/es-CO';

import { IonicModule } from '@ionic/angular';

import { DocumentoPage } from './documento.page';

registerLocaleData(localeEsCo, 'es-CO');


const routes: Routes = [
  {
    path: '',
    component: DocumentoPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  providers: [ { provide: LOCALE_ID, useValue: 'es-CO' } ],
  declarations: [DocumentoPage]
})
export class DocumentoPageModule {}
