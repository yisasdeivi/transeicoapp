import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { VehiculoService } from 'src/app/servicios/vehiculo.service';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import {AyudaPage} from '../ayuda/ayuda.page';




@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
   cedula;
  

  constructor(private  router:  Router,
    private vehiculoservice: VehiculoService,
    private alertcont: AlertController,
    public loadingController: LoadingController,
    private modalcontroller:ModalController) { }

  ngOnInit() {
  }

consulta(){
  console.log(this.cedula);
  
}
async validarExistencia(){
  const loading = await this.loadingController.create({
    message: 'Cargando',
  });
  await loading.present();
  this.vehiculoservice.getPlacas(this.cedula).subscribe(async(placa) =>{
     if(placa.length>=1){
      this.router.navigate(['menu',this.cedula]);
      
     }else{      
     this.presentAlert();      
      console.log('no se encuentra');
     }
  });
  await loading.dismiss();
}
async presentAlert() {
  const alert = await this.alertcont.create({
    header: 'Alerta',
    subHeader: 'Error ',
    message: '<b>Su cedula o Nit no se encuentra en nuestra base de datos</b>',
    buttons: ['OK']
  });

  await alert.present();
}

async abrirModal(){
  const modal = await this.modalcontroller.create({
    component: AyudaPage,
    componentProps: {
   
    }
  });
  return await modal.present();
}


}
