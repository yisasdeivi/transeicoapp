import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VehiculoService } from 'src/app/servicios/vehiculo.service';
import {Placa} from '../../interfaces/placa';
import { Router } from  "@angular/router";
import { LoadingController } from '@ionic/angular';



@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
   placas: Placa[]=[];
   placasven: Placa[]=[];
   cedula =null;
   nombre='';
  constructor(public loadingController: LoadingController,private activatedRoute: ActivatedRoute,private vehiculoservice:VehiculoService, private router :Router) { }

  ngOnInit() {
   
    this.cedula= this.activatedRoute.snapshot.paramMap.get('cedula');
    this.getPlacasVencido();
    this.getPlaca();
    
    
    
    
  }

  async getPlaca() {
    const loading = await this.loadingController.create({
      message: 'Cargando',
    });
    await loading.present();
    this.vehiculoservice.getPlacas(this.cedula)
    .subscribe(async (placa) => { 
      this.nombre=placa[0].nombre;
      this.placas=placa;
      this.comprobarPlaca();
     
    });
    await loading.dismiss();
  
  }
  comprobarPlaca(){    
    for (let index = 0; index < this.placas.length; index++) {
     
      this.placasven.forEach(placa => {       
        if (this.placas[index].placa == placa.placa) {
          console.log('placa a vencer'+placa.placa);          
            this.placas.splice(index,1);
          console.log(this.placas); 
        }
      });      
    }
  }
 getPlacasVencido(){
    
    this.vehiculoservice.getVehiculosvencidos(this.cedula)
    .subscribe(async (vehiculos)=>{
      vehiculos.forEach(vehiculo => {        
        console.log(this.nombre);
           const placa={
           'placa': vehiculo.placa,
           'nombre': vehiculo.nombre
         }
        this.placasven.push(placa);
      
      });

    });  
    

  }

  getdocvencido(placa: string){
    this.router.navigate(['documento',placa]);
    
  }    
}
