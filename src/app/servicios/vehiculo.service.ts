import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Placa } from '../interfaces/placa';
import { Vehiculo } from '../interfaces/vehiculo';


@Injectable({
  providedIn: 'root'
})
export class VehiculoService {
  private url = 'http://notificacion.transeicosas.com/api';

  constructor(private http: HttpClient  ) { }
    


getPlacas(cedula: string){
  const path = `${this.url}/vehiculos?cedula=${cedula}`;
  return this.http.get<Placa[]>(path);
}

getVehiculosvencidos(cedula : string){
  const path = `${this.url}/datosvehiculos?cedula=${cedula}`;
  return this.http.get<Vehiculo[]>(path);
}

getPlacaVehiculoxmes(placa: string){
  const path = `${this.url}/buscarxplacamesvencido?placa=${placa}`;
  return this.http.get<Vehiculo>(path);   
    
} 

getplacaVehiculo(placa: string){
  const path = `${this.url}/buscarxplaca?placa=${placa}`;
  return this.http.get<Vehiculo>(path);
}




}

